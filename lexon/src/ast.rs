use pest::Parser;
use pest::iterators::Pair;
use pest::iterators::Pairs;
use regex::Regex;
use serde::Serialize;
use std::{iter,mem};
use strum_macros::{Display,EnumString};
use std::string::ToString;

#[derive(Parser)]
#[grammar = "lexon.pest"]
pub struct LexonParser;


#[derive(Debug,Clone,Serialize)]
pub struct Symbol{
    pub sym:String,
    pub given:bool,
    pub filler:String
}

#[derive(Debug,Clone,Serialize)]
pub enum MultDiv{
    Mult,
    Div
}
#[derive(Debug,Clone,Serialize)]
pub enum PlusMinus{
    Plus,
    Minus
}
#[derive(Debug,Clone,Serialize)]
pub enum Factor{
    Sym(Symbol),
    Power{b:String,e:String},
    Integer(i32),
    Type(Type),
    Time(String),
    Remainder(String),
}
#[derive(Debug,Clone,Serialize)]
pub struct Term{
    pub ops:Vec<MultDiv>,
    pub factors:Vec<Factor>
}

#[derive(Debug,Clone,Serialize)]
pub struct Expression{
    pub ops:Vec<PlusMinus>,
    pub terms:Vec<Term>
}
#[derive(Debug,Clone,Serialize)]
pub enum BoolOp{
    And,
    Or
}
#[derive(Debug,Clone,Serialize)]
pub enum CmpOp{
    NotEqual,     // !=
    Equal,        // ==
    Greater,      // >
    Less,         // <
    GreaterEqual, // >=
    LessEqual,    // <=
}
#[derive(Debug,Clone,Serialize)]
pub enum TimeCmp{
    Now,
    Past,
    Future,
    NowOrPast,
    NowOrFuture
}

#[derive(Debug,Clone,Serialize)]
pub struct CmpExp{
    pub op:CmpOp,
    pub exp1:Expression,
    pub exp2:Expression
}
#[derive(Debug,Clone,Serialize)]
pub enum BoolStmt{
    Cmp{op:CmpOp,exp1:Expression,exp2:Expression},
    Is(String,String,Expression),
    Date(String,String,TimeCmp)
}
#[derive(Debug,Clone,Serialize)]
pub struct BooleanExpression{
    pub ops:Vec<BoolOp>,
    pub exprs:Vec<BoolStmt>
}

#[derive(Debug,Clone,Serialize,EnumString,Display)]
pub enum Type{
    UndefType,
    Amount,
    Person,
    Key,
    Data,
    Time,
    Text,
    Binary,
    Asset,
    Contract
}


#[derive(Debug,Serialize)]
pub enum Defs{
    Undef,
    Certified,
    Appointed,
    Fixed,
    Deemed,
    Terminated,
    Notified
}


#[derive(Debug,Serialize)]
pub enum Stmt{
    UndefStmt,
    Comment(String),
    Definition{_type:Type},
    Be{def:Defs,expression:Option<Expression>},
    With(String,Option<Expression>),
    Set,
    Pay{who:Option<Symbol>,from:Option<Symbol>,exp:Expression,to:Symbol},
    Return(Vec<Expression>,Symbol),
    Fix(Option<Expression>),
    Appoint,
    Reveal(String,Vec<String>),
    May(String,Vec<Statement>),
    Subtract(String,Expression),
    Add(String,Expression),
    Increase(String,Expression),
    Decrease(String,Expression),
    If{cond:BooleanExpression,ontrue:Vec<Statement>,onfalse:Vec<Statement>},
    For{what:String,statements:Vec<Statement>},
    Sequence,
    And(String),
    Terminate,
    Certify(String,Vec<String>),
    Deem,
    ClauseCall(String,Vec<String>)
}

#[derive(Debug,Serialize)]
pub struct Statement{
    pub stmt:Stmt,
    pub fillers:Vec<String>,
    pub original:String,
    pub varnames:Vec<String>//TODO: move this to each Stmt
}


fn str2type(_t:String)->Type{
    //println!("_tt: {:?}",_tt );
    return match _t.to_lowercase().as_str(){
        "amount"  =>Type::Amount,
        "person"  =>Type::Person,
        "key"     =>Type::Key,
        "data"    =>Type::Data,
        "time"    =>Type::Time,
        "text"    =>Type::Text,
        "binary"  =>Type::Binary,
        "asset"   =>Type::Asset,
        "this contract"=>Type::Contract,
        _=> Type::UndefType
    }
}
fn str2def(_t:String)->Defs{
    return match _t.to_lowercase().as_str(){
        "certified" =>Defs::Certified,
        "appointed" =>Defs::Appointed,
        "fixed"     =>Defs::Fixed,
        "deemed"    =>Defs::Deemed,
        "terminated"=>Defs::Terminated,
        "notified"  =>Defs::Notified,
        _ => Defs::Undef
    }
}
//TODO: replace all symbol matches with this macro!

macro_rules! sym{
    ($e:expr) => {
        {
            let mut r=String::new();
            for s in $e{
                match s.as_rule(){
                    Rule::sym => {                    
                        r=s.as_str().to_string();
                    },
                    _ => {}
                }
            }
            r
        }
    };
    ($e:expr,$v:expr) => {
        for s in $e{
            match s.as_rule(){
                Rule::sym => {                    
                    $v=s.as_str().to_string();
                },
                _ => {}
            }
        }
    }
}
macro_rules! syms{ 
    ($e:expr,$v:expr) => {
        for s in $e{
            match s.as_rule(){
                Rule::sym => {                    
                    $v.push(s.as_str().to_string());
                },
                _ => {}
            }
        }
    }
}

macro_rules! symbol{ 
    ($e:expr) => {
        {
            let mut sym=Symbol{sym:String::new(),given:false,filler:String::new()};
            for s in $e{
                match s.as_rule(){
                    Rule::filler => {
                        sym.filler=s.as_str().to_string();
                    },
                    Rule::given => {
                        sym.given=true;
                    },
                    Rule::sym => { 
                        sym.sym=s.as_str().to_lowercase().to_string();
                    },
                    _ => {}
                }
            };
            sym
        }
    }
}

impl Statement {
    pub fn new()->Statement{
        let fillers:Vec<String>=Vec::new();
        let varnames:Vec<String>=Vec::new();
        let original=String::new();
        let stmt=Stmt::UndefStmt;

        Statement{stmt,fillers,original,varnames}
    }
    pub fn get_stmt(&mut self,stmt_pair: Pair<Rule>,mut payable:&mut bool,mut ret:&mut Vec<String>){

        //self.get_varnames(stmt_pair);
        let rule=stmt_pair.as_rule();
        //println!("rule {:#?}",rule);
        self.original=stmt_pair.as_str().to_string();
        match rule{
            Rule::definition => {
                self.stmt=self.get_definition(stmt_pair);
            },
            Rule::be => {
                self.stmt=self.get_be(stmt_pair);
            },
            Rule::subtract => {
                let mut exp:Option<Expression>=None;
                for _stmt in stmt_pair.into_inner(){
                    let subrule=_stmt.as_rule();
                    match subrule{
                        Rule::symbol => {
                            syms!(_stmt.into_inner(),&mut self.varnames);
                        },
                        Rule::expression => {
                            exp=Some(self.get_expr(_stmt));
                        },
                        _ => {}
                    }
                };
                let from=self.varnames.first().unwrap();
                self.stmt=Stmt::Subtract(from.to_string(),exp.unwrap());
            },
            Rule::add => {
                let mut exp:Option<Expression>=None;
                for _stmt in stmt_pair.into_inner(){
                    let subrule=_stmt.as_rule();
                    match subrule{
                        Rule::symbol => {
                            syms!(_stmt.into_inner(),&mut self.varnames);
                        },
                        Rule::expression => {
                            exp=Some(self.get_expr(_stmt));
                        }
                        ,
                        _ => {}
                    }
                };
                let to=self.varnames.first().unwrap();
                self.stmt=Stmt::Add(to.to_string(),exp.unwrap());
            },
            Rule::increase => {
                let mut exp=None;
                for _stmt in stmt_pair.into_inner(){
                    let subrule=_stmt.as_rule();
                    match subrule{
                        Rule::symbol => {
                            syms!(_stmt.into_inner(),&mut self.varnames);
                        },
                        Rule::expression => {
                            exp=Some(self.get_expr(_stmt));
                        }
                        _ => {}
                    }
                };
                let what=self.varnames.first().unwrap().to_string();
                self.stmt=Stmt::Increase(what,exp.unwrap());//should be safe
            },
            Rule::decrease => {
                let mut exp=None;
                for _stmt in stmt_pair.into_inner(){
                    let subrule=_stmt.as_rule();
                    match subrule{
                        Rule::symbol => {
                            syms!(_stmt.into_inner(),&mut self.varnames);
                        },
                        Rule::expression => {
                            exp=Some(self.get_expr(_stmt));
                        }
                        _ => {}
                    }
                };
                let what=self.varnames.first().unwrap().to_string();
                self.stmt=Stmt::Decrease(what,exp.unwrap());//should be safe
            },
            Rule::appoint => {
                self.get_varnames(stmt_pair);
                self.stmt=Stmt::Appoint;
            },
            Rule::set => {
                self.get_varnames(stmt_pair);
                self.stmt=Stmt::Set;
            },  
            Rule::with => {
                let mut what=String::new();
                let mut expr=None;
                for _stmt in stmt_pair.into_inner(){
                    let subrule=_stmt.as_rule();
                    match subrule{
                        Rule::filler => self.fillers.push(_stmt.as_str().to_string()),
                        Rule::symbol => {
                            for sym in _stmt.into_inner(){
                                match sym.as_rule(){
                                    Rule::filler => self.fillers.push(sym.as_str().to_string()),
                                    Rule::sym => {
                                        self.varnames.push(sym.as_str().to_string());
                                        what=sym.as_str().to_string();
                                    },
                                    _ => {}
                                }
                            }
                        }

                        Rule::_as=> {
                            expr=self.get_as(_stmt);
                        }
                        _ => {}
                    }
                };
                self.stmt=Stmt::With(what,expr);
            },
            Rule::pay => {
                let mut exp=None;
                let mut who: Option<Symbol>=None;
                let mut from:Option<Symbol>=None;
                let mut to:  Option<Symbol>=None;
                for _stmt in stmt_pair.into_inner(){
                    let subrule=_stmt.as_rule();
                    
                    match subrule{
                        Rule::pay_who => {
                            //let mut sym=Symbol{sym:String::new(),given:false,filler:String::new()};
                            let sym=symbol!(_stmt.into_inner().next().unwrap().into_inner());
                            who=Some(sym);
                        }
                        Rule::pay_from => {
                            //let mut sym=Symbol{sym:String::new(),given:false,filler:String::new()};
                            let sym=symbol!(_stmt.into_inner().next().unwrap().into_inner());          
                            from=Some(sym);
                        }
                        Rule::pay_to => {
                            //let mut sym=Symbol{sym:String::new(),given:false,filler:String::new()};
                            let sym=symbol!(_stmt.into_inner().next().unwrap().into_inner());
                            //println!("pay to {:#?}",_stmt.into_inner().next() );
                            to=Some(sym);
                        }
                        Rule::expression => {
                            exp=Some(self.get_expr(_stmt));
                        },
                        _ => {}
                    }
                };
                let exp=exp.unwrap();
                let to=to.unwrap();
       		if to.sym=="escrow" {
			*payable=true;
		}
       		self.stmt=Stmt::Pay{who,from,exp,to};
		// *stateful=true;
            },
            Rule::_return => {
                //println!("return: {:#?}",stmt_pair);
                //let mut to=String::new();
                let mut ret_to:  Option<Symbol>=None;
                let mut what:Vec<Expression>=Vec::new();
                for _stmt in stmt_pair.into_inner(){
                    let subrule=_stmt.as_rule();
                    match subrule{
                        Rule::symbol => {
                            //let mut sym=Symbol{sym:String::new(),given:false,filler:String::new()};
                            let sym=symbol!(_stmt.into_inner());
                            ret_to=Some(sym);
                            //for sym in _stmt.into_inner(){
                            //    match sym.as_rule(){
                            //        Rule::filler => self.fillers.push(sym.as_str().to_string()),
                            //        Rule::sym => {
                            //            to=sym.as_str().to_string();
                            //        },
                            //        _ => {}
                            //    }
                            //}
                        },   
                        Rule::expression => {
                            what.push(self.get_expr(_stmt));
                        },
                        
                        _ => {}
                    }
                };
                self.stmt=Stmt::Return(what,ret_to.unwrap());
            }
            Rule::fix => {
                //println!("fix{:#?}", stmt_pair);
                let mut e:Option<Expression>=None;
                for _stmt in stmt_pair.into_inner(){                    
                    match _stmt.as_rule(){
                        Rule::symbol => {
                            for sym in _stmt.into_inner(){
                                match sym.as_rule(){
                                    Rule::filler => self.fillers.push(sym.as_str().to_string()),
                                    Rule::sym => {
                                        self.varnames.push(sym.as_str().to_string());
                                    },
                                    _ => {}
                                }
                            }
                        },   
                        Rule::_as=> {
                            e=self.get_as(_stmt);
                        },
                        _ => {}
                    }
                };
                self.stmt=Stmt::Fix(e);
            },
            Rule::reveal => {
                //self.get_varnames(stmt_pair);
                let mut to=String::new();
                let mut list:Vec<String>=Vec::new();
                for _stmt in stmt_pair.into_inner(){                    
                    match _stmt.as_rule(){
                        Rule::symbol => {
                            for sym in _stmt.into_inner(){
                                match sym.as_rule(){
                                    Rule::filler => self.fillers.push(sym.as_str().to_string()),
                                    Rule::sym => {
                                        to=sym.as_str().to_string();
                                    },
                                    _ => {}
                                }
                            }
                        },   
                        Rule::reveal_list=> {
                            for li in _stmt.into_inner(){
                                match li.as_rule(){
                                    Rule::symbol => {
                                        for sym in li.into_inner(){
                                            match sym.as_rule(){
                                                Rule::filler => self.fillers.push(sym.as_str().to_string()),
                                                Rule::sym => {
                                                    list.push(sym.as_str().to_string());
                                                    ret.push(sym.as_str().to_string());
                                                },
                                                _ => {}
                                            }
                                        }
                                    },
                                    _ => {}
                                }
                            }
                        },
                        _ => {}
                    }
                };
                
                self.stmt=Stmt::Reveal(to,list);
            },
            Rule::may=>{
                let mut statements:Vec<Statement>=Vec::new();
                let mut who=String::new();
                for _stmt in stmt_pair.into_inner(){
                    let subrule=_stmt.as_rule();
                    match subrule{                        
                        Rule::symbol => {
                            for sym in _stmt.into_inner(){
                                match sym.as_rule(){
                                    Rule::filler => self.fillers.push(sym.as_str().to_string()),
                                    Rule::sym => {
                                        who=sym.as_str().to_lowercase();
                                    },
                                    _ => {}
                                }
                            }
                        },
                        Rule::maystmts => {
                            get_statements(_stmt.into_inner(),&mut statements,&mut payable,&mut ret);
                        }
                        _ => {}
                    }
                };
                self.stmt=Stmt::May(who,statements);
            },
            Rule::ifstmt=>{
                let mut cond:Option<BooleanExpression>=None;
                let mut ontrue:Vec<Statement>=Vec::new();
                let mut onfalse:Vec<Statement>=Vec::new();
                for _stmt in stmt_pair.into_inner(){
                    let subrule=_stmt.as_rule();
                    match subrule{
                        Rule::condition => {
                            let boolexpression=_stmt.into_inner().next().unwrap();
                            //println!("cond: {:#?}",boolexpression);
                            cond=Some(self.get_boolexpr(boolexpression));
                        },
                        Rule::ontrue => {
                            get_statements(_stmt.into_inner().next().unwrap().into_inner(),&mut ontrue,&mut payable,&mut ret);
                        },
                        Rule::onfalse => {
                            get_statements(_stmt.into_inner().next().unwrap().into_inner(),&mut onfalse,&mut payable,&mut ret);
                        }
                        _ => {}
                    }
                };
                self.stmt=Stmt::If{cond:cond.unwrap(),ontrue,onfalse};
            }
            Rule::seq  => {
                self.stmt=Stmt::Sequence;
            },
            Rule::commasep => {
                self.stmt=Stmt::And(",".to_string())
            },
            Rule::_and => {
                self.stmt=Stmt::And(",".to_string())
            },
            Rule::comment => {
                let cmt=stmt_pair.as_str().to_string();
                self.stmt=Stmt::Comment(cmt);
            },
            Rule::terminate => {
                self.stmt=Stmt::Terminate;
            },
            Rule::certify => {
                //self.get_varnames(stmt_pair);
                let mut s=String::new();
                let mut list:Vec<String>=Vec::new();
                for _stmt in stmt_pair.into_inner(){                    
                    match _stmt.as_rule(){                        
                        Rule::symbol => {
                            sym!(_stmt.into_inner(),s);
                        },
                        Rule::_certify => {
                            for _c in _stmt.into_inner(){ 
                                match _c.as_rule(){
                                    Rule::symbol => {
                                        let mut l=String::new();
                                        sym!(_c.into_inner(),l);
                                        list.push(l);
                                    },
                                    _=>{}
                                }
                            }
                        },
                        _=>{}
                    }
                }
                //println!("certify!!! {:?}",s );
                //println!("certify!!! {:?}",list );
                self.stmt=Stmt::Certify(s,list);
            },
            Rule::clausecall=>{
                let mut name=String::new();
                let mut params:Vec<String>=Vec::new();
                for _stmt in stmt_pair.into_inner(){                    
                    match _stmt.as_rule(){                        
                        Rule::clausename => {
                            let _cn=_stmt.into_inner().next().unwrap();
                            sym!(_cn.into_inner(),name);
                        },
                        Rule::parameters => {
                            for _p in _stmt.into_inner(){                                
                                params.push(sym!(_p.into_inner()));
                            }
                        }                     
                        _=>{}
                    }
                }
                self.stmt=Stmt::ClauseCall(name,params);
            }
            _ => ()
        };
        //Statement{stmt,fillers,varnames}
    }
    fn get_definition(&mut self,stmt_pair: Pair<Rule>)->crate::ast::Stmt{
        let mut _type:Type=Type::UndefType;
        
        for _stmt in stmt_pair.into_inner(){
            let subrule=_stmt.as_rule();
            match subrule{
                Rule::varnameq => {
                    for s in _stmt.into_inner(){
                        if let Rule::syms = s.as_rule(){
                            //println!("  varnameq: {:?}", s.as_str().to_string());
                            self.varnames.push(s.as_str().to_string())
                        }
                    }
                }
                Rule::_type => {
                    let s=_stmt.as_str();
                    let t=s.clone().to_lowercase();
                    _type=str2type(t);
                },
                _ => {}
            }
        };
        Stmt::Definition{_type}
    }
    fn get_be(&mut self,stmt_pair: Pair<Rule>)->crate::ast::Stmt{
        let mut def=Defs::Undef;
        let mut expression=None;
        for _stmt in stmt_pair.into_inner(){
            let subrule=_stmt.as_rule();
            match subrule{
                Rule::symbol => {
                    //self.varnames.push(_stmt.as_str().to_string())
                    for sym in _stmt.into_inner(){
                        match sym.as_rule(){
                            Rule::filler => self.fillers.push(sym.as_str().to_string()),
                            Rule::sym => {
                                self.varnames.push(sym.as_str().to_string());                                
                            },
                            _ => {}
                        }
                    }
                },
                Rule::_def => {
                    let s=_stmt.as_str();
                    let d=s.clone().to_lowercase();
                    def=str2def(d);
                },
                Rule::_as=> {
                    expression=self.get_as(_stmt);
                }
                _ => {}
            }
        };
        Stmt::Be{def,expression}
    }
    fn get_varnames(&mut self,stmt_pair: Pair<Rule>){
        for _stmt in stmt_pair.into_inner(){
            let subrule=_stmt.as_rule();
            match subrule{
                Rule::filler => self.fillers.push(_stmt.as_str().to_string()),
                Rule::symbol => {        
                    for sym in _stmt.into_inner(){
                        match sym.as_rule(){
                            Rule::filler => self.fillers.push(sym.as_str().to_string()),
                            Rule::sym => {
                                self.varnames.push(sym.as_str().to_string())                                
                            },
                            _ => {}
                        }
                    }
                },
                _ => {}
            }
        }
    }
    fn get_as(&mut self,stmt_pair: Pair<Rule>)->Option<Expression>{
        let mut expr=None;
        for _as in stmt_pair.into_inner(){
            match _as.as_rule(){
                Rule::filler => self.fillers.push(_as.as_str().to_string()),
                Rule::expression => {
                    expr=Some(self.get_expr(_as))
                },
                _ => {}
            }
        }
        expr
    }
    fn get_expr(&mut self,stmt_pair: Pair<Rule>)->Expression{
        let mut ops:Vec<PlusMinus>=Vec::new();
        let mut terms:Vec<Term>  =Vec::new();
        for term in stmt_pair.into_inner(){
            match term.as_rule(){
                Rule::term => terms.push(self.get_terms(term)),
                Rule::plusminus=>
                    match term.as_str().to_lowercase().as_str() {
                        "plus"  => ops.push(PlusMinus::Plus),
                        "minus" => ops.push(PlusMinus::Minus),
                        _ => {}
                    }
                ,
                _ => {}
            }
        }
        Expression{ops,terms}
    }
    fn get_terms(&mut self,stmt_pair: Pair<Rule>)->Term{
        let mut ops:Vec<MultDiv>=Vec::new();
        let mut factors:Vec<Factor>=Vec::new();

        for term in stmt_pair.into_inner(){
            //println!("term {:#?}",term);
            match term.as_rule(){
                Rule::factor=> {
                    //factor ={ "(" ~ expression ~ ")" | power|_type| remainder | time| sum | integer | symbol }
                    for factor in term.into_inner(){
                        //println!("factor {:#?}",factor);
                        match factor.as_rule(){
                            Rule::power=>{
                                //factors.push(factor.as_str().to_string())
                                let mut b=String::new();
                                let mut e=String::new();
                                for pwr in factor.into_inner(){
                                    match pwr.as_rule(){
                                        Rule::base=>{
                                            b=pwr.as_str().to_string()
                                        },
                                        Rule::exponent=>{
                                            e=pwr.as_str().to_string()
                                        },
                                        _ => {}
                                    }
                                }
                                factors.push(Factor::Power{b,e})
                            },
                            Rule::remainder=>{
                                for rem in factor.into_inner(){
                                    match rem.as_rule(){
                                        Rule::symbol=>{
                                            for s in rem.into_inner(){
                                                match s.as_rule(){
                                                    Rule::sym=>{                                                        
                                                        factors.push(Factor::Remainder(s.as_str().to_string()))
                                                    },
                                                    _ => {}
                                                }
                                            }
                                            //factors.push(Factor::Symbol(rem.as_str().to_string()))
                                        },
                                        _ => {}
                                    }
                                }
                            },
                            Rule::integer=>{
                                //let my_string =factor.as_str().to_string() 
                                match factor.as_str().trim().parse::<i32>(){
                                    Ok(i)=> {
                                        factors.push(Factor::Integer(i))
                                    },
                                    _ => {
                                        println!("cant convert str to int '{}'",factor.as_str())
                                    }
                                }
                            },
                            Rule::symbol=>{
                                let sym=symbol!(factor.into_inner());
                                factors.push(Factor::Sym(sym))
                            },
                            Rule::_type=>{
                                factors.push(Factor::Type(str2type(factor.as_str().to_string())))
                            },
                            Rule::time=>{
                                //println!("time: {:#?}",factor.as_str());
                                factors.push(Factor::Time("now".to_string()))
                            },
                            _ => {}
                        }
                    }
                },
                Rule::multdiv=>
                    match term.as_str().to_lowercase().as_str() {
                        "times"=> ops.push(MultDiv::Mult),
                        "*"=> ops.push(MultDiv::Mult),
                        "divided by"=> ops.push(MultDiv::Div),
                        "/"=> ops.push(MultDiv::Div),
                        _ => {}
                    }
                ,
                _ => {}
            }
        }
        Term{ops,factors}
    }
    fn get_boolexpr(&mut self,stmt_pair: Pair<Rule>)->BooleanExpression{
        let mut ops:Vec<BoolOp>=Vec::new();
        let mut exprs:Vec<BoolStmt>=Vec::new();
        for bexp in stmt_pair.into_inner(){
            match bexp.as_rule(){
                Rule::boolop=>{
                    match bexp.as_str(){
                        "and"=> ops.push(BoolOp::And),
                        "or"=> ops.push(BoolOp::Or),
                        _ => ()    
                    };
                }
                Rule::cmp=>{
                    let mut op:Option<CmpOp>=None;
                    let mut exp:Vec<Expression>=Vec::new();
                    for b in bexp.into_inner(){
                        match b.as_rule(){
                            Rule::expression=>{
                                exp.push(self.get_expr(b));
                            },
                            Rule::le=>{
                                op=Some(CmpOp::Less);
                            },
                            Rule::gt=>{
                                op=Some(CmpOp::Greater);
                            },
                            Rule::leq=>{
                                op=Some(CmpOp::LessEqual);
                            },
                            Rule::geq=>{
                                op=Some(CmpOp::GreaterEqual);
                            },
                            Rule::ne=>{
                                op=Some(CmpOp::NotEqual);
                            },
                            Rule::eq=>{
                                op=Some(CmpOp::Equal);
                            },
                            _=>{}
                        }
                    }
                    let exp2=exp.pop().unwrap();
                    let exp1=exp.pop().unwrap();
                    exprs.push(BoolStmt::Cmp{op:op.unwrap(),exp1,exp2});
                },
                Rule::date=>{
                    println!("date {:#?}",bexp );
                    let mut symbol=String::new();
                    let mut timecmp:Option<TimeCmp>=None;
                    let mut cmpop=String::new();
                    
                    for date in bexp.into_inner(){
                        match date.as_rule(){
                            Rule::symbol=>{
                                for sym in date.into_inner(){
                                    match sym.as_rule(){
                                        Rule::sym => {
                                            symbol= sym.as_str().to_string();
                                        },
                                        _ => {}
                                    }
                                }
                            },
                            Rule::_is=>{
                                cmpop="is".to_string();
                            },
                            Rule::_isnot=>{
                                cmpop="isnot".to_string();
                            },
                            Rule::now=>{
                                timecmp=Some(TimeCmp::Now);
                            },
                            Rule::future=>{
                                timecmp=Some(TimeCmp::Future);
                            },
                            Rule::past=>{
                                timecmp=Some(TimeCmp::Past);
                            },
                            Rule::now_or_future=>{
                                timecmp=Some(TimeCmp::NowOrFuture);
                            },
                            Rule::now_or_past=>{
                                timecmp=Some(TimeCmp::NowOrPast);
                            },
                            _=>{}
                        }
                    }
                    match timecmp{
                        Some(t)=>{
                            exprs.push(BoolStmt::Date(symbol,cmpop,t));
                        },None=>{}
                    }
                },
                Rule::expr_is => {
                    println!("expr_is {:#?}",bexp );
                    let mut cmpop=String::new();
                    let mut symbol=String::new();
                    let mut e:Option<Expression>=None;
                    for expr_is in bexp.into_inner(){
                        match expr_is.as_rule(){
                            Rule::expression=>{
                                e=Some(self.get_expr(expr_is));
                            },
                            Rule::sym => {
                                symbol= expr_is.as_str().to_string();
                            },
                            Rule::_is=>{
                                cmpop="is".to_string();
                            },
                            Rule::_isnot=>{
                                cmpop="isnot".to_string();
                            },
                            _ => {}
                        }
                    }
                    exprs.push(BoolStmt::Is(symbol,cmpop,e.unwrap()));
                },
                _ => {}
            }
        }
        BooleanExpression{ops,exprs}
    }
}

#[derive(Debug,Serialize)]
pub struct Chapter{
    pub name: String,
    pub payable: bool,
    pub statements:Vec<Statement>,
    pub ret:Vec<String>
}

#[derive(Debug,Serialize)]
pub struct Contract{
    pub name: String,
    pub statements:Vec<Statement>,
    pub chapters:Vec<Chapter>
}

#[derive(Debug,Serialize)]
pub struct Lexon{
    pub name:String,
    pub version:String,
    pub comments:Vec<String>,
    pub toplevel_payable:bool,
    pub term_payable:bool,
    pub term_stmts:Vec<Statement>,
    pub term_chpts:Vec<Chapter>,
    pub contracts:Vec<Contract>
}

fn get_statements(pairs: Pairs<Rule>,terms:&mut Vec<Statement>,payable:&mut bool,ret:&mut Vec<String>){
    for stmt in pairs{
        let mut s=Statement::new();
        s.get_stmt(stmt,payable,ret);
        terms.push(s);
    }
}

fn get_clauses(pairs: Pairs<Rule>,chpts:&mut Vec<Chapter>,toplevel_payable:&mut bool){
    for _chpts in pairs{
        match _chpts.as_rule(){
            Rule::clause => {
                let mut name=String::new();
                let mut clause_payable=false;
                let mut statements:Vec<Statement>=Vec::new();
                let mut ret:Vec<String>=Vec::new();
                for _chpt in _chpts.into_inner(){
                    match _chpt.as_rule(){
                        Rule::clausename => {
                            for _n in _chpt.into_inner(){
                                match _n.as_rule(){
                                    Rule::symbol => {
                                        name=_n.as_str().to_string();
                                    },
                                    _ => {}
                                }
                            }
                        },
                        Rule::stmts => {
                           get_statements(_chpt.into_inner(),&mut statements,&mut clause_payable,&mut ret);
			   *toplevel_payable = *toplevel_payable || clause_payable;
                        },
                        _ => {}
                    }
                };
                chpts.push(Chapter{name,statements,payable:clause_payable,ret}); 
            },
            _ => {}
        }
    };
}

impl Lexon {
    fn new(pairs: Pairs<Rule>)->Lexon{
        let mut name="".to_string();
        let mut version="0.2.12".to_string();
        let mut comments:Vec<String>=Vec::new();
        let mut toplevel_payable:bool=false;
        let mut ret:Vec<String>=Vec::new();
        let mut term_payable:bool=false;
        let mut term_stmts:Vec<Statement>=Vec::new();
        let mut term_chpts:Vec<Chapter>=Vec::new();
        let mut contracts:Vec<Contract>=Vec::new();
        for lexon in pairs{
            //println!("  rule {:?}", lexon.as_rule());
            match lexon.as_rule(){
                Rule::lex => { 
                    println!("  lex: {:#?}", lexon);
                    //unwrap should be safe cos parser ensures name rule exists
                    name=lexon.into_inner().next().unwrap().as_str().to_string();                   
                },
                Rule::lexver => { 
                    lexon.into_inner().map(|lxv|{
                        version=lxv.as_str().to_string();
                    }).count();
                },
                Rule::terms => {
                    for terms in lexon.into_inner(){
                        match terms.as_rule(){
                            Rule::stmts=> {
				get_statements(terms.into_inner(),&mut term_stmts,&mut term_payable,&mut ret);
			   	toplevel_payable = toplevel_payable || term_payable;
			    },
                            Rule::clauses => {
				get_clauses(terms.into_inner(),&mut term_chpts,&mut toplevel_payable);
			    },
                            _ => {}
                        }
                    }
                },
                Rule::comments => {
                    lexon.into_inner().map(|cmt|{
                        comments.push(cmt.as_str().to_string());
                    }).count();
                }
                Rule::contracts => {
                    //println!("contracts {:#?}", lexon);
		    let mut name="".to_string();
		    let mut statements:Vec<Statement>=Vec::new(); 
		    let mut chapters:Vec<Chapter>=Vec::new(); 
                    for _ctrs in lexon.into_inner(){
                        match _ctrs.as_rule(){
                            Rule::name => {
                               name=_ctrs.as_str().to_string();
                            },
                            Rule::stmts => {
                                get_statements(_ctrs.into_inner(),&mut statements,&mut toplevel_payable,&mut ret);
                            },
                            Rule::clauses => {
                                //println!("clauses!!");
                                get_clauses(_ctrs.into_inner(),&mut chapters,&mut toplevel_payable);
                            },
                            _ => {}
                        }
                    };
		    contracts.push(Contract{name,statements,chapters});
                },
                _ => unreachable!()
            };
        };
        //if terms.len()==0){
        //    panic!("no terms!");
        //}
        Lexon{name,version,comments,toplevel_payable,term_payable,term_stmts,term_chpts,contracts}
    }
}

fn check_vars(code: String) -> String{
    //println!("original code: {:#?}",code);
    let mut code2=code.clone();
    
    code2=code2.replace("‘","x").as_str().to_string();
    code2=code2.replace("“","\"").as_str().to_string();

    let mut out=code2.clone();
    let re = Regex::new(r#"(?mx)[“""]([^”""]+).*$"#).unwrap();
    let re2 = Regex::new(r#"(?mi)clause\s*:\s*([^\.]+)\."#).unwrap();
    
    let mut vars:Vec<String>=Vec::new();
    let mut clauses:Vec<String>=Vec::new();
    for cap in re.captures_iter(&code2){
        vars.push(cap[1].to_string());
    }
    for cap in re2.captures_iter(&code2){
        vars.push(cap[1].to_string());
        clauses.push(cap[1].to_string()); 
    }
    //out=code.clone();
    for var in vars{
        let re = Regex::new(&var).unwrap();
        out=re.replace_all(&out,var.replace(" ","_").as_str().replace("’", "__").as_str()).to_string();
    }

    for clause in clauses{
        let cl=clause.replace(" ","_");
        
        //println!("clause: {:#?}",cl);
        
        let re = Regex::new(&cl).unwrap();
        let r=format!("&{}",cl);
        out=re.replace_all(&out,r.as_str()).to_string();
    }
    let lines:Vec<String>=vec!(out.split("\\n").collect());
    println!("Preprocessed Lexon code:");
    for i in lines {
        println!("{}", i);
    }
    
    out
}

pub fn parse(unparsed_file:String)->Result<Vec<Lexon>,pest::error::Error<crate::ast::Rule>>{
    let unparsed_file=check_vars(unparsed_file.clone());

    let parse_tree = LexonParser::parse(Rule::lexons, &unparsed_file);
    let mut lexons:Vec<Lexon>=Vec::new();
    // println!("Pest AST: {:#?}", parse_tree);
    match parse_tree {
        Ok(pairs) => {
            //println!("{}",pretty_parse_tree(pairs));
            for lexon in pairs{
                match lexon.as_rule(){
                    Rule::lexon => { 
                        lexons.push(Lexon::new(lexon.into_inner()))
                    },
                    _ => {}
                }
            };
            Ok(lexons)
        },
        Err(error) => {
            //println!("{}",pretty_parse_tree(parse_tree));
            Err(error)
        }
    }
}

//pub fn print(unparsed_file:String)->String{
//    format!("lexons= {:#?}", parse(unparsed_file))
//}
pub fn get_json(lexons:&Vec<Lexon>)->String{
    //let lexons=parse(unparsed_file);
    match serde_json::to_string(lexons){
        Ok(s) => s,
        Err(e) => format!("{:?}",e )
    }
}

pub fn pretty_parse_tree(pairs: Pairs<Rule>) -> String {
    #[derive(Clone)]
    struct State {
        depth: usize,
        prefix: String,
    };

    fn go(pairs: Pairs<Rule>, lines: &mut Vec<String>, mut state: State) {
        if state.prefix.is_empty() {
            let indent: String = iter::repeat("  ").take(state.depth).collect();
            state.prefix = format!("{}- ", indent);
        }

        for pair in pairs.into_iter() {
            let rule = format!("{:?}", pair.as_rule());

            let repr = String::from(pair.as_str());
            let pairs = pair.into_inner();

            if pairs.peek().is_some() {
                mem::drop(repr);

                let children: Vec<_> = pairs.clone().into_iter().collect();
                let child_count = children.len();

                if child_count == 1 {
                    let mut state = state.clone();
                    state.prefix = format!("{}{} > ", state.prefix, rule);
                    go(pairs, lines, state);
                } else {
                    lines.push(format!("{}{}", state.prefix, rule));

                    let mut state = state.clone();
                    state.depth += 1;
                    state.prefix = String::new();

                    go(pairs, lines, state);
                }
            } else {
                lines.push(format!("{}{}: {:?}", state.prefix, rule, repr));
            }
        }
    }

    let mut lines = vec![];
    go(pairs, &mut lines, State {
        depth: 0,
        prefix: String::new(),
    });
    lines.join("\n")
}
