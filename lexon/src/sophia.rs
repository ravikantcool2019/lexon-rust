use crate::ast::*;
use crate::vm::*;

pub fn type2str(_tt:&Type)->String{
    //println!("_tt: {:?}",_tt );
    let t=match _tt{
        Type::Amount=>"int", 
        Type::Person=>"address",
        Type::Key=>"address",
        Type::Data=>"hash",
        Type::Time=>"int",
        Type::Text=>"string",
        Type::Binary=>"bool",
        Type::Asset=>"hash",
        Type::Contract=>"",//special case to get this contract
        _=>"//err"
    };
    return t.to_string()
}

pub fn sop_operand(op:&Operand,variables:&Vec<Variable>,params:&Vec<Variable>)-> String {
    match op{
        Operand::Val(v)=>{
            match v{
                Value::Id(i)=>{
                    i.to_string()
                },
                Value::Date(d)=>{
                    d.to_string()
                },
                Value::Binary(b)=>{
                    match b{
                        Some(v)=>v.to_string(),
                        None=> "false".to_string()
                    }
                },
                Value::Integer(i)=>{
                    i.to_string()
                },
                Value::ThisAddress=>{
                    "Contract.address".to_string()
                },
                Value::MsgValue=>"Call.value".to_string()
            }
        },
        Operand::Var(v)  => {
            //if v=="escrow" {
            //    "Contract.balance".to_string()
            //}else{
                v.to_string()
           // }
        },
        Operand::Par(p)  => p.to_string(),
        Operand::Exp(e)  => sop_expr(&e,variables,params),
        Operand::Func(f) => f.to_string()
    }
}

pub fn sop_operations(ops:&Vec<Operation>,sop:&mut Vec<String>,variables:&Vec<Variable>,params:&Vec<Variable>,mays:usize){
    //let mut ret="".to_string();
    let mut ret_list:Vec<String>=Vec::new();
    let mut may=0;
    for op in ops{
        match op{
            Operation::Transfer{who,from: _,exp,to}=>{
                let to=sop_operand(to ,variables,params);
                let  e=sop_operand(exp,variables,params);
                match who{
                    Some(_)=>{ // w
                        //println!("exp {:#?}",e );
                        //let w=sop_operand(w,variables);
                        sop.push(format!("Chain.spend({}, {}) // :y:",sop_varname(&to),e))
                    },
                    None => sop.push(format!("Chain.spend({}, {})",sop_varname(&to),e))
                };
            },
            Operation::Assign(op1,op2)=>{
                let dest=sop_operand(op1,variables,params);
                let what=sop_operand(op2,variables,params);
                sop.push(format!("{} = {},",dest,what));  
            },
            Operation::Reveal(list)=>{
                ret_list=list.into_iter().map(|op|
                    sop_operand(op,variables,params)
                ).collect();
            },
            Operation::Terminate=>{
                sop.push(format!("selfdestruct(address(0));"));
            },
            Operation::May(op_who,ops)=>{
                let who=sop_operand(op_who,variables,params);
                
                sop.push(format!("{}if(Call.caller == {})",
                    if may==0 {""} else {"else "},
                    sop_varname(&who)));
                sop.push(format!(">")); 

                sop_operations(ops,sop,variables,params,0);
                
                if mays == 0 {
                    sop.push(format!("<"));
                }else{
                    if may==mays-1 {
                        sop.push(format!("<"));
                    }
                    may+=1;
                }
                 
            },
            Operation::If(cond,ontrue,onfalse)=>{
                let c=sop_bexpr(cond,variables,params);
                if onfalse.len()>0 {
                    sop.push(format!("if( {} )",c));
                    sop.push(format!(">"));
                }else{
                    sop.push(format!("require( {} );",c));
                }
                
                sop_operations(&ontrue,sop,variables,params,0);
                
                if onfalse.len()>0 {            
                    sop.push(format!("<")); 
                    sop.push(format!("else")); 
                    sop.push(format!(">")); 
                    sop_operations(&onfalse,sop,variables,params,0);
                    sop.push(format!("<")); 
                }
                  
            },
            Operation::Inc(dest,e)=>{
                let dest=sop_operand(dest,variables,params);
                let e=sop_operand(e,variables,params);
                sop.push(format!("{}+={};",dest,e));  
            },
            Operation::Dec(dest,e)=>{
                let dest=sop_operand(dest,variables,params);
                let e=sop_operand(e,variables,params);
                sop.push(format!("{}-={};",dest,e));  
            },
            Operation::Sub(from,e)=>{
                let from=sop_operand(from,variables,params);
                let e=sop_operand(e,variables,params);
                sop.push(format!("{}-={};",from,e));  
            },
            Operation::Add(to,e)=>{
                let to=sop_operand(to,variables,params);
                let e=sop_operand(e,variables,params);
                sop.push(format!("{}+={};",to,e));
            },
            Operation::Call(func,cparams)=>{
                let func=sop_operand(func,variables,params);
                let cparams:Vec<String>=cparams.iter().map(|p|
                    sop_operand(p,variables,params)
                ).collect();
                sop.push(format!("{}({});",func,cparams.join(",")));  
            }
        }
    }
    if ret_list.len()>0 {
        sop.push(format!("return ({});",ret_list.join(",")));
    }
}

impl LexonVM{

    pub fn sophia(&self)->String{
        let mut sop:Vec<String>=Vec::new();
        for program in &self.programs{
            
            for comment in &program.comments {
            	sop.push(format!(""));
                sop.push(format!("/* {} */",comment.trim()));
            }

            sop.push(format!(""));
            sop.push(format!("{}contract {} =",
		if program.payable { "payable " } else { "" },	
		program.name.replace(" ","")));
            sop.push(format!(""));
          
	    let mut i:usize  = 0; 
            for var in &program.variables {
		if i == 0 {
	            sop.push(format!(">"));
        	    sop.push(format!("record state ="));
 		}
                sop.push(format!("{} {} : {}{}",
			if i == 0 { "{" } else { " " },
			var.name.to_lowercase(),type2str(&var._type),
			if i+1 < program.variables.len() { "," } else { " }" }));
		i += 1;
	    }
	    if i > 0 {
                sop.push(format!(""));
	    }
	 
            for obj in &program.objects {
		    let mut i:usize  = 0; 
		    for var in &obj.variables {
			if i == 0 {
			    sop.push(format!("record {} =", obj.name));
			}
			sop.push(format!("{} {} : {}{}",
				if i == 0 { "{" } else { " " },
				var.name.to_lowercase(),type2str(&var._type),
				if i+1 < obj.variables.len() { "," } else { " }" }));
			i += 1;
		    }
		    if i > 0 {
			sop.push(format!(""));
		    }
	    }	
 
	    // init function (the constructor)

            let params:Vec<String>=program.constructor.parameters.iter().map(|v|  
                format!("{} : {}",v.name.to_lowercase(),type2str(&v._type))
            ).collect();
 
            sop.push(format!("public {}stateful entrypoint init({}) =",
		if program.constructor.payable { "payable " } else { "" },
		params.join(", ")));
            sop.push(format!(">"));
 
       	    let short = sop_assignments_only(&mut sop,&program.constructor.operations,
		&program.variables, &program.constructor.parameters); 
	    let mut paren = false;

	    if !short {
		sop.push(format!("let init_state ="));
		sop.push(format!(">"));
	    }

     	    let _caller=program.variables.iter()
		.find(|x:&&Variable| 
                    match x._type { 
			Type::Person if x.bound => {
		            if short {
			    	sop.push(format!("{{ {} = Call.caller,",
					 &x.name));
	    			paren = true;
			    } else {
			        sop.push(format!("let {} = Call.caller", &x.name));
			    }
			    true
			},
			_ => false
                    }
		);

	    i = 0; 
	    let mut max = program.constructor.operations.len();
	    for op in &program.constructor.operations {
		match op{
			Operation::Assign(op1,op2)=>{
				let l=sop_operand(op1,&program.variables,
					&program.constructor.parameters);
				let r=sop_operand(op2,&program.variables,
					&program.constructor.parameters);
				sop.push(format!("{} {} = {}{}",
				if i == 0 && !paren { "{" } else { " " },
				l,r,
				if i+1 < max { 
					"," } else { " }" }));
				i += 1;
			},
			_=> {
				max = max - 1
			}
		}
	    }

	    if !short {		
		    for op in &program.constructor.operations {
			match op{
				Operation::Assign(_op1,_op2)=>{},
				_=>{
					sop_operations(&program.constructor.operations,&mut sop,
						&program.variables,
						&program.constructor.parameters,0);
				}
			}
		    }
		    if !short {
			sop.push(format!("init_state"));
		    }
	    }
	    sop.push(format!("<"));

	    if i == 0 { sop.push(format!("{{}}")); }

	    sop.push(format!("<"));


            //functions

            for func in &program.terms_functions{
                let mut ret:Vec<String>=Vec::new();
                for r in &func.returns{
                    if let Some(_va) = program.variables.iter().position(
                        |x| x.name==r.name.to_lowercase() 
                    )
                    {
                        ret.push(type2str(&r._type));
                    }
                }
                let mut ret_s=String::new();
                if ret.len()>0{
                  ret_s=format!(": {}", ret.join(","))
                }
                let access=(if func.is_private { "private"} else { "public" })
			.to_string();
                let params:Vec<String>= func.parameters.iter().map(|p|
                    //p.name.to_string()
                    format!("{} {}",type2str(&p._type),p.name.to_lowercase())
                ).collect();
                let params=params.join(",");
                sop.push(format!(""));
                sop.push(format!("{} {}stateful entrypoint {}({}) {} =",
			access,
			if func.payable { "payable " } else { "" },
       			func.name,params,ret_s));
                sop.push(format!(">"));
                
                sop_operations(&func.operations,&mut sop,&program.variables,&func.parameters,func.mays.len());
                                
                sop.push(format!("<"));
            }
            sop.push(format!("<"));
        }

        //println!("{:#?}",sop );
        const INDENT_WITH:usize=4;        
        let mut indent_level:i8=0;
        let mut next_indent_level:i8=0;
        let mut out:Vec<String>=Vec::new();
        for s in &sop{
	    indent_level += next_indent_level;
	    next_indent_level = 0;
            let mut indent="".to_string();
	    if s.len()>0 {	
                if s == "{" {
                    next_indent_level+=1;
                }
                if s == ">" {
                    next_indent_level+=1;
                }
                if s == "}" {
                    next_indent_level-=1;
                }
                if s == "<" {
                    next_indent_level-=1;
                }
                if indent_level>0 {
                    indent=format!("{:width$}"," ",width=indent_level as usize*INDENT_WITH);
                }
	   }
            if s != "<" && s != ">" {
                out.push(format!("{}{}",indent,s));
	    }
        }

	sop.push(format!("<"));
        out.join("\n")
    }
}

fn sop_varname(varname:&String)->String{
    if varname.trim().to_lowercase() == "escrow"{
        "Contract.balance".to_string()
    }else{
        "state.".to_string() + &varname.trim().to_lowercase()
    }
}

fn sop_expr(expr: &Expression,variables:&Vec<Variable>,params:&Vec<Variable>)->String{
    let mut output=String::new();
    let mut t=0;
    let t_ops_len=expr.ops.len();
    for term in &expr.terms{
        let mut f=0;
        let f_ops_len=term.ops.len();
        for factor in &term.factors{
            match factor{
                Factor::Sym(symbol)=>{
                    let symbol=symbol.sym.to_lowercase();
                    if variables.iter().any(|v| &v.name==&symbol ){
                      output+=&sop_varname(&symbol);                      
                    }else if params.iter().any(|v| &v.name==&symbol ){
                      output+=&sop_varname(&symbol);                      
                    }else{
                        match symbol.as_str(){
                            "true"=>{
                                output+=&format!(" {} ",symbol);
                            },
                            "false"=>{
                                output+=&format!(" {} ",symbol);
                            },
                            "escrow"=>{
                                output+=&format!(" Contract.balance ");
                            },
                            //"Amount"=>{
                            //    output+=&format!(" msg.value ");
                            //},
                            _=>{
                              //text?
                                output+=&format!(" '{}' ",symbol.to_lowercase());
                                //output+=&sop_varname(symbol);
                            }    
                        }
                    }
                    
                },
                Factor::Remainder(symbol)=>{
                  output+=&sop_varname(symbol);
                }
                Factor::Power{b,e}=>{
                    output+=&format!(" {}**{} ",b,e);
                },
                Factor::Integer(i)=>{
                    output+=&format!(" {} ",i);
                },
                Factor::Time(t)=>{
                    output+=&format!(" {} ",t);
                },
                Factor::Type(t)=>{
                    match t{
                        Type::Contract =>{
                            output+=&format!(" this ");
                        },
                        _ =>{

                        }
                    }
                }
            }
            if f < f_ops_len{
                match &term.ops[f]{
                    MultDiv::Mult=> {
                        output+=&format!(" * ");
                    },
                    MultDiv::Div=> {
                        output+=&format!(" / ");
                    },
                }
            }
            f+=1;
        }
        if t < t_ops_len{
            match &expr.ops[t]{
                PlusMinus::Plus=> {
                    output+=&format!(" + ");
                },
                PlusMinus::Minus=> {
                    output+=&format!(" - ");
                },
            }
        }
        t+=1;
    }
    output
}

fn sop_bexpr(b: &BooleanExpression,variables:&Vec<Variable>,params:&Vec<Variable>)->String{
    let mut output=String::new();
    //let mut i=0;
    let c=b.ops.len();
    //let e=b.exprs.len();
    for (i, exp) in b.exprs.iter().enumerate(){

        match exp{
            BoolStmt::Cmp{op,exp1,exp2}=>{
                let e1=sop_expr(exp1,variables,params);
                output.push_str(&e1);
                match op{
                    CmpOp::Less        =>output.push_str(" < "),
                    CmpOp::Greater     =>output.push_str(" > "),
                    CmpOp::NotEqual    =>output.push_str(" != "),
                    CmpOp::Equal       =>output.push_str(" == "),
                    CmpOp::GreaterEqual=>output.push_str(" >= "),
                    CmpOp::LessEqual   =>output.push_str(" <= ")
                }
                let e2=sop_expr(exp2,variables,params);
                output.push_str(&e2);
            },
            BoolStmt::Date(sym,cmp,time)=>{
              match cmp.as_str(){
                "isnot"=>{
                  output.push_str("!");
                },
                _=>{}
              };
              match time{
                TimeCmp::Now=>{
                  output+=&format!("({} == now )",sym.to_lowercase());
                },
                TimeCmp::Past=>{
                  output+=&format!("({} > now )",sym.to_lowercase());
                },
                TimeCmp::Future=>{
                  output+=&format!("({} > now )",sym.to_lowercase());
                },
                TimeCmp::NowOrPast=>{
                  output+=&format!("({} <= now )",sym.to_lowercase());
                },
                TimeCmp::NowOrFuture=>{
                  output+=&format!("({} >= now )",sym.to_lowercase());
                }
              }
            },
            BoolStmt::Is(sym,cmp,exp)=>{
              let ex=&sop_expr(exp,variables,params);
              match cmp.as_str(){
                "is"=>{                  
                  output+=&format!("({} == {} )",sop_varname(sym),&ex);
                },
                "isnot"=>{                  
                  output+=&format!("({} != {} )",sop_varname(sym),&ex);
                },
                _=>{}
              };              
            }
        };
        if i < c{
            match b.ops[i]{
                BoolOp::And => output.push_str(" && "),
                BoolOp::Or  => output.push_str(" || ")
            }
        }
        //i+=1
    }
    output
}

fn sop_assignments_only(_sop:&mut Vec<String>,ops:&Vec<Operation>,variables:&Vec<Variable>,parameters:&Vec<Variable>)->bool{

	for op in ops{
		match op{
			Operation::Assign(_op1,_op2)=>{
            			//4: _sop.push(format!("A"));
			},
			Operation::Transfer{who:_,from:_,exp:_,to}=> 
				if sop_operand(&to,variables,parameters) != "escrow" {
            				//4: _sop.push(format!("T"));
					return false;
				},
			_=> {
            			//4: _sop.push(format!("Other"));
				return false;
			}	
		}
	}
	return true;
}
