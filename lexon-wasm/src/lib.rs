
use lexon::*;
use crate::ast::*;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern {
    #[allow(dead_code)]
    fn log(s: &str);
    #[allow(dead_code)]
    fn error(e: &str);
}

#[allow(unused_macros)]
macro_rules! log {
    ($($t:tt)*) => (log(&format_args!($($t)*).to_string()))
}

#[allow(unused_macros)]
macro_rules! error {
    ($($t:tt)*) => (error(&format_args!($($t)*).to_string()))
}

#[wasm_bindgen]
#[derive(Debug)]
pub struct LexonWasm {
    lexon:String,
    vm:Option<vm::LexonVM>
}
#[wasm_bindgen]
impl LexonWasm {
    #[wasm_bindgen(constructor)]
    pub fn new() -> LexonWasm {
        LexonWasm {
            lexon:"0.2".to_string(),
            vm:None
        }
    }
    pub fn compile(&mut self,code:&str)->String{
    	match parse(code.to_string()){
    		Ok(lexons)=>{
                let vm=vm::LexonVM::new(lexons);
                
    			//self.lexons=Some(lexons);
                self.vm=Some(vm);
    			"ok".to_string()
    		},
    		Err(_error)=>{
    			//log!("error: {:?}",error);
    			_error.to_string()
    		}
    	}
    }
    pub fn solidity(&mut self)->String{
        match &self.vm{
            Some(vm)=>vm.solidity(),
            _=> "".to_string()
        }
    }
    pub fn sophia(&mut self)->String{
        match &self.vm{
            Some(vm)=>vm.sophia(),
            _=> "".to_string()
        }
    }
    pub fn english(&mut self)->String{
        match &self.vm{
            Some(vm)=>vm.english(),
            _=> "".to_string()
        }
    }
    pub fn args(&mut self)->String{
        match &self.vm{
            Some(vm)=>
                vm.get_args()
            ,
            _=> "".to_string()
        }
    }
    pub fn json(&mut self)->String{
        match &self.vm{
            Some(vm)=>vm.get_json(),
            _=> "".to_string()
        }
    }
}
